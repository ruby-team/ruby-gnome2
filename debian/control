Source: ruby-gnome
Section: ruby
Priority: optional
Maintainer: Debian Ruby Team <pkg-ruby-extras-maintainers@lists.alioth.debian.org>
Uploaders: Youhei SASAKI <uwabami@gfd-dennou.org>
Build-Depends: debhelper-compat (= 13),
               gem2deb,
               dctrl-tools,
               rake-compiler,
               ruby-mechanize,
               libgirepository1.0-dev (>= 1.62.0-4~),
               libffi-dev (>= 3.3),
               libgstreamer-plugins-base1.0-dev,
               libgstreamer1.0-dev,
               libgtksourceview-3.0-dev,
               libgtk-4-dev,
               libjpeg-dev,
               libpng-dev,
               librsvg2-dev,
               libxmu-dev,
               pkgconf,
               ruby-cairo,
               ruby-pkg-config (>= 1.3.5)
Standards-Version: 4.6.2
Homepage: https://ruby-gnome2.osdn.jp/
Vcs-Browser: https://salsa.debian.org/ruby-team/ruby-gnome
Vcs-Git: https://salsa.debian.org/ruby-team/ruby-gnome.git
XS-Ruby-Versions: all
Rules-Requires-Root: no

Package: ruby-glib2
Architecture: any
XB-Ruby-Versions: ${ruby:Versions}
X-DhRuby-Root: glib2
Depends: ruby-pkg-config (>= 1.3.5),
         ${misc:Depends},
         ${shlibs:Depends},
         ${ruby:Depends}
Multi-Arch: same
Description: GLib 2 bindings for the Ruby language
 GLib is a useful general-purpose C library, notably used by GTK+ and GNOME.
 .
 This package contains libraries for using GLib 2 with the Ruby programming
 language. It is most likely useful in conjunction with Ruby bindings for
 other libraries such as GTK+.

Package: ruby-atk
Architecture: all
XB-Ruby-Versions: ${ruby:Versions}
X-DhRuby-Root: atk
Depends: gir1.2-atk-1.0,
         ruby-gobject-introspection (>= ${source:Version}),
         ruby-glib2 (>= ${source:Version}),
         ${misc:Depends},
         ${ruby:Depends}
Multi-Arch: foreign
Description: ATK bindings for the Ruby language
 ATK is a toolkit providing accessibility interfaces for applications or other
 toolkits. By implementing these interfaces, those other toolkits or
 applications can be used with tools such as screen readers, magnifiers, and
 other alternative input devices.
 .
 This package contains libraries for using ATK with the Ruby programming
 language.

Package: ruby-cairo-gobject
Architecture: any
XB-Ruby-Versions: ${ruby:Versions}
X-DhRuby-Root: cairo-gobject
Depends: ruby-cairo,
         ruby-glib2 (= ${binary:Version}),
         ${misc:Depends},
         ${shlibs:Depends},
         ${ruby:Depends}
Multi-Arch: same
Description: CairoGObject bindings for the Ruby language
 Cairo is a multi-platform library providing anti-aliased vector-based
 rendering for multiple target backends; CairoGObject provides
 wrapper GObject types for all Cairo types.
 .
 This package contains libraries for CairoGObject with the Ruby language.

Package: ruby-gdk-pixbuf2
Architecture: all
XB-Ruby-Versions: ${ruby:Versions}
X-DhRuby-Root: gdk_pixbuf2
Depends: gir1.2-gdkpixbuf-2.0,
         ruby-gobject-introspection (>= ${source:Version}),
         ruby-gio2 (>= ${source:Version}),
         ${misc:Depends},
         ${ruby:Depends}
Multi-Arch: foreign
Description: Gdk-Pixbuf 2 bindings for the Ruby language
 Gdk-Pixbuf is a library for loading and rendering images.
 .
 This package contains libraries for using Gdk-Pixbuf with the Ruby programming
 language.

Package: ruby-gdk3
Architecture: all
XB-Ruby-Versions: ${ruby:Versions}
X-DhRuby-Root: gdk3
Depends: gir1.2-gtk-3.0,
         ruby-atk (>= ${source:Version}),
         ruby-cairo-gobject (>= ${source:Version}),
         ruby-gdk-pixbuf2 (>= ${source:Version}),
         ruby-gobject-introspection (>= ${source:Version}),
         ruby-pango (>= ${source:Version}),
         ${misc:Depends},
         ${ruby:Depends}
Multi-Arch: foreign
Description: GDK 3 bindings for the Ruby language
 GDK is a computer graphics library that acts as a wrapper around the
 low-level drawing and windowing functions provided by the underlying
 graphics system.  It is mainly use by GTK+ 3.
 .
 This package contains libraries for using GDK 3 with the Ruby programming
 language.

Package: ruby-gdk4
Architecture: all
XB-Ruby-Versions: ${ruby:Versions}
X-DhRuby-Root: gdk4
Depends: gir1.2-gtk-4.0,
         ruby-cairo-gobject (>= ${source:Version}),
         ruby-gdk-pixbuf2 (>= ${source:Version}),
         ruby-pango (>= ${source:Version}),
         ${misc:Depends},
         ${ruby:Depends}
Multi-Arch: foreign
Description: GDK 4 bindings for the Ruby language
 GDK is a computer graphics library that acts as a wrapper around the
 low-level drawing and windowing functions provided by the underlying
 graphics system.  It is mainly use by GTK+ 4.
 .
 This package contains libraries for using GDK 4 with the Ruby programming
 language.

Package: ruby-gio2
Architecture: any
XB-Ruby-Versions: ${ruby:Versions}
X-DhRuby-Root: gio2
Depends: gir1.2-glib-2.0,
         ruby-glib2 (= ${binary:Version}),
         ruby-gobject-introspection (= ${binary:Version}),
         ${misc:Depends},
         ${shlibs:Depends},
         ${ruby:Depends}
Multi-Arch: same
Description: GIO bindings for the Ruby language
 GIO is striving to provide a modern, easy-to-use VFS API that sits at the
 right level in the library stack, as well as other generally useful APIs for
 desktop applications such as networking and D-Bus support.
 .
 This package provides GIO bindings to be used in Ruby programs.

Package: ruby-gnome
Architecture: all
Depends: ruby-atk,
         ruby-cairo-gobject,
         ruby-gdk-pixbuf2,
         ruby-gdk3,
         ruby-gio2,
         ruby-gstreamer,
         ruby-gtk3,
         ruby-gtksourceview4,
         ruby-pango,
         ruby-poppler,
         ruby-rsvg2,
         ${misc:Depends}
Breaks: ruby-gnome2 (<< 3.3.8)
Replaces: ruby-gnome2 (<< 3.3.8)
Multi-Arch: foreign
Description: GNOME-related bindings for the Ruby language
 These bindings allow use of the GNOME developer platform using the Ruby
 programming language.
 .
 This is an empty package that depends on the various packages that provide
 the individual bindings.

Package: ruby-gnome-dev
Architecture: any
Section: libdevel
Depends: ruby-all-dev,
         ruby-gnome,
         ruby-pkg-config,
         ${misc:Depends}
Breaks: ruby-gnome2-dev (<< 3.3.8)
Replaces: ruby-gnome2-dev (<< 3.3.8)
Multi-Arch: same
Description: GNOME-related bindings for the Ruby language (development files)
 These bindings allow use of the GNOME developer platform using the Ruby
 programming language.
 .
 This package contains development files required to build ruby-gnome
 extensions.

Package: ruby-gobject-introspection
Architecture: any
XB-Ruby-Versions: ${ruby:Versions}
X-DhRuby-Root: gobject-introspection
Depends: ruby-glib2 (= ${binary:Version}),
         ${misc:Depends},
         ${shlibs:Depends},
         ${ruby:Depends}
Multi-Arch: same
Description: GObject Introspection bindings for the Ruby language
 GObject Introspection is a project for providing machine readable
 introspection data of the API of C libraries.  This introspection data can
 be used in several different use cases, for example automatic code
 generation for bindings, API verification and documentation generation.
 .
 This package contains library for using GObject Introspection with the Ruby
 programming language.

Package: ruby-gstreamer
Architecture: any
XB-Ruby-Versions: ${ruby:Versions}
X-DhRuby-Root: gstreamer
Depends: gir1.2-gstreamer-1.0,
         ruby-atk (>= ${source:Version}),
         ruby-gdk-pixbuf2 (>= ${source:Version}),
         ruby-gobject-introspection (= ${binary:Version}),
         ruby-pango (= ${binary:Version}),
         ${misc:Depends},
         ${shlibs:Depends},
         ${ruby:Depends}
Multi-Arch: same
Description: GStreamer bindings for the Ruby language
 GStreamer is a media processing framework with support for a wide variety of
 data sources, sinks, and formats through the use of dynamically loaded
 plugins.
 .
 This package contains libraries for using GStreamer with the Ruby programming
 language.

Package: ruby-gtk3
Architecture: any
XB-Ruby-Versions: ${ruby:Versions}
X-DhRuby-Root: gtk3
Depends: ruby-atk (>= ${source:Version}),
         ruby-gdk3 (>= ${source:Version}),
         ${misc:Depends},
         ${shlibs:Depends},
         ${ruby:Depends}
Multi-Arch: same
Description: GTK+ 3 bindings for the Ruby language
 GTK+ is a multi-platform toolkit for creating graphical user interfaces.
 Offering a complete set of widgets, GTK+ is suitable for projects ranging
 from small one-off tools to complete application suites.
 .
 This package contains libraries for using GTK+ 3 with the Ruby programming
 language.

Package: ruby-gtk4
Architecture: any
XB-Ruby-Versions: ${ruby:Versions}
X-DhRuby-Root: gtk4
Depends: ruby-atk (>= ${source:Version}),
         ruby-gdk4 (>= ${source:Version}),
         ${misc:Depends},
         ${shlibs:Depends},
         ${ruby:Depends}
Multi-Arch: same
Description: GTK+ 4 bindings for the Ruby language
 GTK+ is a multi-platform toolkit for creating graphical user interfaces.
 Offering a complete set of widgets, GTK+ is suitable for projects ranging
 from small one-off tools to complete application suites.
 .
 This package contains libraries for using GTK+ 4 with the Ruby programming
 language.

Package: ruby-gtksourceview4
Architecture: all
XB-Ruby-Versions: ${ruby:Versions}
X-DhRuby-Root: gtksourceview4
Depends: gir1.2-gtksource-4,
         ruby-gtk3 (>= ${source:Version}),
         ${misc:Depends},
         ${ruby:Depends}
Multi-Arch: foreign
Description: GtkSourceView4 bindings for the Ruby language
 GTKSourceView4 is a text widget that extends the standard GTK+ text widget.
 .
 This package contains libraries for using the GtkSourceView4 text widget
 with syntax highlighting and other features typical of a source code
 editor in the Ruby programming language.

Package: ruby-pango
Architecture: any
XB-Ruby-Versions: ${ruby:Versions}
X-DhRuby-Root: pango
Depends: gir1.2-pango-1.0,
         ruby-gobject-introspection (= ${binary:Version}),
         ruby-cairo-gobject (= ${binary:Version}),
         ${misc:Depends},
         ${shlibs:Depends},
         ${ruby:Depends}
Multi-Arch: same
Description: Pango bindings for the Ruby language
 Pango is a library for layout and rendering of text, with an emphasis on
 internationalization.
 .
 This package contains libraries for using Pango with the Ruby programming
 language.

Package: ruby-poppler
Architecture: all
XB-Ruby-Versions: ${ruby:Versions}
X-DhRuby-Root: poppler
Depends: gir1.2-poppler-0.18,
         ruby-gobject-introspection (>= ${source:Version}),
         ruby-gio2 (>= ${source:Version}),
         ruby-cairo-gobject (>= ${source:Version}),
         ${misc:Depends},
         ${ruby:Depends}
Multi-Arch: foreign
Description: Ruby bindings for the libpoppler-glib library
 Poppler is a PDF rendering library based on xpdf PDF viewer.
 libpoppler-glib is Glib binding for Poppler.
 .
 This package contains ruby bindings for libpoppler-glib.

Package: ruby-rsvg2
Architecture: all
XB-Ruby-Versions: ${ruby:Versions}
X-DhRuby-Root: rsvg2
Depends: gir1.2-rsvg-2.0,
         ruby-glib2 (>= ${source:Version}),
         ruby-gio2 (>= ${source:Version}),
         ruby-gdk-pixbuf2 (>= ${source:Version}),
         ruby-cairo-gobject (>= ${source:Version}),
         ruby-pango (>= ${source:Version}),
         ${misc:Depends},
         ${ruby:Depends}
Multi-Arch: foreign
Description: RSVG renderer bindings for the Ruby language
 The rsvg library is an efficient renderer for Scalable Vector Graphics (SVG)
 pictures.
 .
 This package contains libraries for using RSVG with the Ruby programming
 language.

Package: ruby-webkit2-gtk
Architecture: all
XB-Ruby-Versions: ${ruby:Versions}
X-DhRuby-Root: webkit2-gtk
Depends: gir1.2-webkit2-4.1,
         ruby-gobject-introspection (>= ${source:Version}),
         ruby-gtk3 (>= ${source:Version}),
         ${misc:Depends},
         ${ruby:Depends}
Multi-Arch: foreign
Description: WebKitGTK+ bindings for the Ruby language
 WebKitGTK+ is the GNOME platform port of the WebKit rendering engine.
 Offering WebKit’s full functionality through a set of GObject-based APIs,
 it is suitable for projects requiring any kind of web integration, from
 hybrid HTML/CSS applications to full-fledged web browsers, like Epiphany
 and Midori.
 .
 This package contains libraries for using WebKitGTK+ with GTK+ 3 and the Ruby
 language.
